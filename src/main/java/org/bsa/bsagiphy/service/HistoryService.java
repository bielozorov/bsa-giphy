package org.bsa.bsagiphy.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import org.bsa.bsagiphy.dto.History;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Service
public class HistoryService {

    private final CsvMapper csvMapper = new CsvMapper();

    @Value("${app.users.dir}")
    private String users;

    public List<History> getUserHistory(String userId) {

        Path dirPath = Path.of(users + userId),
                filePath = Path.of(dirPath + "/history.csv");

        if (!Files.exists(filePath) || !Files.exists(dirPath)) {
            throw new RuntimeException("Cannot get gif file");
        }
        File csvFile = new File(filePath.toString());

        try {

            MappingIterator<History> historyIter =
                    csvMapper.readerWithTypedSchemaFor(History.class).readValues(csvFile);

            List<History> result = historyIter.readAll();
            if (result.isEmpty()) {
                throw new RuntimeException("Cannot get gif file");
            }
            return result;

        } catch (IOException e) {
            throw new RuntimeException("Cannot read values from history");
        }

    }

    public void deleteUserHistory(String userId) {

        Path dirPath = Path.of(users + userId),
                filePath = Path.of(dirPath + "/history.csv");

        if (!Files.exists(filePath) || !Files.exists(dirPath)) {
            throw new RuntimeException("Cannot get gif file");
        }
        try {

            PrintWriter pw = new PrintWriter(filePath.toString()); // clean history file
            pw.close();

        } catch (IOException e) {
            throw new RuntimeException("Could not delete history");
        }

    }
}
