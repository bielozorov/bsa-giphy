package org.bsa.bsagiphy.dto;

import lombok.Data;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

@Data
public class GifDto {
    private final String query;
    private final Set<Path> gifs = new HashSet<>();
}
