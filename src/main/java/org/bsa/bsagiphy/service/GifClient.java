package org.bsa.bsagiphy.service;

import org.bsa.bsagiphy.dto.GifWithImageDto;

public interface GifClient {
    GifWithImageDto getGif(String query);

    byte[] getGifPicture(String url);
}
