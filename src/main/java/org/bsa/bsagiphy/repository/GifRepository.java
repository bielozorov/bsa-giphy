package org.bsa.bsagiphy.repository;

import lombok.Getter;
import org.bsa.bsagiphy.dto.GifDto;
import org.springframework.stereotype.Repository;

import java.nio.file.Path;
import java.util.*;

@Repository
@Getter
public class GifRepository {

    private static final HashMap<String, List<GifDto>> dictionary = new HashMap<>();

    public void addGif(String userId, String query, Path filePath) {
        List<GifDto> list = dictionary.get(userId);
        if (list == null) {
            list = new ArrayList<>();
            GifDto gif = new GifDto(query);
            gif.getGifs().add(filePath);
            list.add(gif);
            dictionary.put(userId, list);
        } else {
            GifDto gifByQuery = findByQuery(list, query);
            if (gifByQuery != null) {
                gifByQuery.getGifs().add(filePath);
            } else {
                GifDto gif = new GifDto(query);
                gif.getGifs().add(filePath);
                list.add(gif);
            }
        }
    }

    private static GifDto findByQuery(List<GifDto> list, String query) {
        return list
                .stream()
                .filter(gif -> gif.getQuery().equals(query))
                .findFirst()
                .orElse(null);
    }

    public Optional<Path> findByQueryInUser(String userId, String query) {
        List<GifDto> list = dictionary.get(userId);
        if (list == null || list.isEmpty()) {
            return Optional.empty();
        }
        GifDto gifByQuery = findByQuery(list, query);
        if (gifByQuery != null) {
            Random random = new Random();
            int length = gifByQuery.getGifs().size();
            return gifByQuery.getGifs().stream().skip(random.nextInt(length)).findFirst();
        }
        return Optional.empty();
    }

    public void deleteByUser(String userId) {
        dictionary.remove(userId);
    }

    public void deleteByQuery(String userId, String query) {
        var list = dictionary.get(userId);
        if (list == null) {
            throw new RuntimeException("Resource not found");
        }
        var gifByQuery = findByQuery(list, query);
        if (gifByQuery != null) {
            list.remove(gifByQuery);
        }
    }
}
