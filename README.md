BSA Giphy
Нужно написать WebAPI приложение, который позволяет генерировать и сохранять персонализированные GIFs. Для получения GIF необходимо использовать API GIPHY.

Приложение состоит из 2 частей: файловое хранилище на диске, и персонализированный кэш в памяти (словарь):

{
 "john": [{
   "ball": [
     "/path/to/1.gif",
     "/path/to/2.gif",
     "/path/to/3.gif",
   ],
   "moon": [
     "/path/to/1.gif",
     "/path/to/2.gif",
     "/path/to/3.gif",
   ]
 }],
 "jack": [{
   "mad": [
     "/path/to/1.gif",
     "/path/to/2.gif",
     "/path/to/3.gif",
   ]
 }]
}
В приложении должны быть реализованы две основные функции:

Генерация GIF
Пользователь отправляет запрос на генерацию GIF. Пользователь должен указать id - может быть любая строка, которую можно использовать в качестве пути в OS (необходимо добавить базовую валидацию по id), а так же ключевое слово, по которому можно найти GIF.
Приложение пытается получить путь к GIF из кэша на диске (папка cache) (если есть несколько подходящих GIF, то выбрать случайную) и добавить его в в папку пользователя (по id). Затем обновить данные в кэше (в памяти) и вернуть пользователю путь к GIF.
Если приложение не нашло GIF на диске (папка cache), то нужно сделать запрос на giphy.com, сохранить GIF (в качестве имени файла можно использовать Id из ответа от giphy.com или UUID) в кэш на диске (папка cache) и выполнить шаг 2.
Добавить запись о генерации в файл истории пользователя history.csv
10-10-2020,mad,/path/to/1.gif
10-12-2020,ball,/path/to/2.gif
Поиск GIF
Пользователь отправляет запрос на поиск GIF. Пользователь должен указать id - может быть любая строка, которую можно использовать в качестве пути в OS (необходимо добавить базовую валидацию по id), а так же ключевое слово, по которому можно найти GIF.
Приложение пытается получить путь к GIF из кэша в памяти (если есть несколько, то выбрать случайную) и вернуть пользователю.
Если в памяти нет подходящей GIF, то попробовать найти в папке пользователя и выполнить шаг 2.
Если в папке пользователя нет подходящей GIF, вернуть 404.
Важно:

Добавить два типа конфигурации: dev и prod. В prod версии не хранить API Key для GIPHY.
Использовать DI для всех элементов веб-приложения.
Добавить в приложение валидацию на хедер X-BSA-GIPHY (значение может быть любым): если хедер отсутствует вернуть 403.
Нужно подключить actuator и посмотреть информацию, которую он возвращает.
Каждый запрос пользователя должен быть залоггирован.
FS structure
bsa_giphy
 cache
   ball
     1.gif
     2.gif
     3.gif
   moon
     1.gif
     2.gif
     3.gif
 users
   john
     history.csv
     ball
       1.gif
       2.gif
       3.gif
   jack
     history.csv
     moon
       1.gif
       2.gif
       3.gif
API
Получить кэш с диска. Если указан query, то выбрать только соответствующие файлы.
GET /cache?query

Response

[
 {
   "query": "ball",
   "gifs": [
     "/path/to/1.gif",
     "/path/to/2.gif",
     "/path/to/3.gif",
   ]
 },
 {
   "query": "mad",
   "gifs": [
     "/path/to/1.gif,
     "/path/to/2.gif,
     "/path/to/3.gif,
   ]
 },
 {
   "query": "moon",
   "gifs": [
     "/path/to/1.gif",
     "/path/to/2.gif",
     "/path/to/3.gif",
   ]
 }
]
Скачать картинку из giphy.com и положить в соответствующую папку в кэше на диске. Вернуть объект со всеми картинками в кэше на диске.
POST /cache/generate

Body

{
 "query": "moon"
}
Response

{
 "query": "moon",
 "gifs": [
   "/path/to/1.gif",
   "/path/to/2.gif",
   "/path/to/3.gif",
 ]
}
Очистить кэш на диске
DELETE /cache

Получить список всех файлов без привязки к ключевым словам
GET /gifs

Response

[
 "/path/to/1.gif",
 "/path/to/2.gif",
 "/path/to/3.gif",
 "/path/to/4.gif"
]
Получить список всех файлов с папки пользователя на диске
GET /user/:id/all

Response

[
   {
     "query": "ball",
     "gifs": [
       "/path/to/1.gif",
       "/path/to/2.gif",
       "/path/to/3.gif",
     ]
   },
   {
     "query": "moon",
     "gifs": [
       "/path/to/1.gif",
       "/path/to/2.gif",
       "/path/to/3.gif",
     ]
   }
]
Получить историю пользователя
GET /user/:id/history

Response

[
 {
   "date": "10-10-2020",
   "query": "mad",
   "gif": "/path/to/1.gif"
 },
 {
   "date": "10-11-2020",
   "query": "ball",
   "gif": "/path/to/2.gif"
 },
 {
   "date": "10-12-2020",
   "query": "moon",
   "gif": "/path/to/3.gif"
 },
]
Очистить историю пользователя
DELETE /user/:id/history/clean

Выполнить поиск картинки. Если указан параметр force, то игнорировать кэш в памяти и сразу читать данные с диска. Добавить файл в кэш в памяти, если его еще там нет.
GET /user/:id/search?query&force

Response

"/path/to/1.gif"
Сгенерировать GIF. Если указан параметр force, то игнорировать кэш на диске (папка cache) и сразу искать файл в giphy.com. Добавить файл в кэш на диске (папка cache). (Доп. задание: избежать дублирования GIF файлов в кэше)
POST /user/:id/generate

BODY

{
 "query": "ball",
 "force": true
}
RESPONSE

"/path/to/1.gif"
Очистить кэш пользователя в памяти по ключу query. Если ключ не указан, то очистить все данные по пользователю в кэше в памяти.
DELETE /user/:id/reset?query

Удалить все данные по пользователю как на диске так и в кэше в памяти
DELETE /user/:id/clean