package org.bsa.bsagiphy.service;

import org.bsa.bsagiphy.dto.GifDto;
import org.bsa.bsagiphy.dto.GifWithImageDto;
import org.bsa.bsagiphy.repository.GifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class GifService {
    private final GifClient gifClient;
    private final GifRepository gifsRepository;

    private final CacheService cacheService;

    @Value("${app.cache.dir}")
    private String cache;

    @Value("${app.users.dir}")
    private String users;

    @Autowired
    public GifService(GifClient gifClient, GifRepository gifsRepository, CacheService cacheOperationService) {
        this.gifClient = gifClient;
        this.gifsRepository = gifsRepository;
        this.cacheService = cacheOperationService;
    }

    public List<GifDto> getAllUserGifs(String userId) {
        List<GifDto> result = new ArrayList<>();
        Path userDirPath = Path.of(users + userId);
        if (!Files.exists(userDirPath)) {
            throw new RuntimeException("Resource not found");
        }
        File dir = new File(userDirPath.toString());
        File[] queryDirs = dir.listFiles();
        if (queryDirs == null) {
            throw new RuntimeException("Resource not found");
        }
        for (File queryDir : queryDirs) {
            if (!queryDir.getName().equals("history.csv")) {
                result.addAll(cacheService.listFilesInDirectory(queryDir));
            }
        }
        return result;
    }

    public GifDto generateGif(String query) {
        downloadFromGiphy(query);
        GifDto gif = new GifDto(query);
        File cacheQueryDir = new File(cache + query);
        File[] files = cacheQueryDir.listFiles();
        if (files == null) {
            throw new RuntimeException("Cannot get gif file");
        }
        gif.getGifs()
                .addAll(Arrays.stream(files)
                        .map(File::toPath)
                        .collect(Collectors.toList()));
        return gif;
    }

    public void downloadFromGiphy(String query) {
        try {
            GifWithImageDto gif = gifClient.getGif(query);
            Path dirPath = Path.of(cache + query);
            Path filePath = Path.of(dirPath + "/" + gif.getId() + ".gif");
            cacheService.createDirectoriesIfNotExist(Path.of(cache), dirPath);
            if (!Files.exists(filePath)) {
                File file = new File(filePath.toString());
                OutputStream os = new FileOutputStream(file);
                os.write(gif.getGif());
                os.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public Path getFromCache(String userId, String query) {
        File dir = new File(cache + query + "/");
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            downloadFromGiphy(query);
            files = dir.listFiles();
        }
        if (files == null) {
            throw new RuntimeException("Cannot get file from cache");
        }
        Random rand = new Random();
        File file = files[rand.nextInt(files.length)];
        String fileName = file.getName();
        Path usersFile = cacheService.writeToUsersDir(userId, query, file.toPath(), fileName);
        gifsRepository.addGif(userId, query, usersFile);
        cacheService.writeHistoryToCsv(userId, query, usersFile);
        return usersFile;
    }

    public Path findGifOnDisk(String userId, String query) {
        Path userDirPath = Path.of(users + userId), queryDirPath = Path.of(userDirPath + "/" + query);
        if (!Files.exists(userDirPath) || !Files.exists(queryDirPath)) {
            throw new RuntimeException("Cannot get gif file");
        }
        File dir = new File(queryDirPath.toString());
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            throw new RuntimeException("Cannot get gif file");
        }
        Random random = new Random();
        File file = files[random.nextInt(files.length)];
        gifsRepository.addGif(userId, query, file.toPath());
        return file.toPath();
    }

    public Path findGif(String userId, String query) {
        var gifPath = gifsRepository.findByQueryInUser(userId, query);
        return gifPath.orElseGet(() -> findGifOnDisk(userId, query));
    }

    public Path getFromGiphy(String userId, String query) {
        downloadFromGiphy(query);
        return getFromCache(userId, query);
    }
}
