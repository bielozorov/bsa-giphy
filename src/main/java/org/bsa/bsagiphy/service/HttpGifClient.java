package org.bsa.bsagiphy.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bsa.bsagiphy.dto.GifWithImageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class HttpGifClient implements GifClient {

    private final ObjectMapper mapper = new ObjectMapper();

    @Value("${api.gifs-api}")
    private String gifsApiUrl;

    @Value("${api.key}")
    private String apiKey;

    private final HttpClient client;


    @Autowired
    public HttpGifClient(HttpClient client) {
        this.client = client;
    }

    @Override
    public GifWithImageDto getGif(String query) {
        try {
            HttpResponse<String> response = client.send(buildGetRequest(query), HttpResponse.BodyHandlers.ofString());
            JsonNode gifInfo = mapper.readTree(response.body());
            JsonNode gifId = gifInfo.path("data").path("id");
            JsonNode gifUrl = gifInfo.path("data").path("images").path("original").path("url");
            if (gifId.toString().isEmpty() || gifUrl.toString().isEmpty()) {
                throw new RuntimeException("Resource not found");
            }
            byte[] image = getGifPicture(gifUrl.asText());
            return new GifWithImageDto(gifId.asText(), image);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Resource not found");
        }
    }

    @Override
    public byte[] getGifPicture(String url) {
        try {
            var response = client.send(buildGetPictureRequest(url), HttpResponse.BodyHandlers.ofByteArray());
            return response.body();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    private HttpRequest buildGetRequest(String query) {
        query = query.replaceAll(" ", "%20");
        return HttpRequest
                .newBuilder()
                .setHeader("X-BSA-GIPHY", "Hello")
                .uri(URI.create(gifsApiUrl + "?tag=" + query + "&api_key=" + apiKey))
                .GET()
                .build();
    }

    private HttpRequest buildGetPictureRequest(String url) {
        return HttpRequest
                .newBuilder()
                .uri(URI.create(url))
                .setHeader("Accept", "image/gif")
                .GET()
                .build();
    }
}