package org.bsa.bsagiphy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;


@ConfigurationPropertiesScan
@SpringBootApplication
// @SpringBootApplication(scanBasePackages = { "com" })
//@EntityScan(basePackages = { "com" })
public class BsaGiphyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsaGiphyApplication.class, args);
    }
}
