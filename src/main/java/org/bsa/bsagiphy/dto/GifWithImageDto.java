package org.bsa.bsagiphy.dto;

import lombok.Data;

@Data
public class GifWithImageDto {
    private final String id;
    private final byte[] gif;
}
