package org.bsa.bsagiphy.controller;

import org.bsa.bsagiphy.dto.GifDto;
import org.bsa.bsagiphy.dto.GifGenerationDto;
import org.bsa.bsagiphy.dto.History;
import org.bsa.bsagiphy.dto.UserIdDto;
import org.bsa.bsagiphy.service.CacheService;
import org.bsa.bsagiphy.service.GifService;
import org.bsa.bsagiphy.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.Path;
import java.util.List;

@Validated
@RestController
@RequestMapping("/user")
public class UserController {

    private final GifService gifService;

    private final HistoryService historyService;

    private final CacheService cacheService;

    @Autowired
    public UserController(GifService gifService, HistoryService historyService, CacheService cacheService) {
        this.gifService = gifService;
        this.historyService = historyService;
        this.cacheService = cacheService;
    }

    @GetMapping("/{userId}/all")
    List<GifDto> getAllUserGifs(@PathVariable @Valid UserIdDto userId) {
        return gifService.getAllUserGifs(userId.getUserId());
    }

    @GetMapping("/{userId}/history")
    List<History> getUserHistory(@PathVariable @Valid UserIdDto userId) {
        return historyService.getUserHistory(userId.getUserId());
    }

    @GetMapping("/{userId}/search")
    public Path findGif(@PathVariable @Valid UserIdDto userId, @RequestParam String query, @RequestParam(required = false) Boolean force) {
        return (force != null && force)
                ? gifService.findGifOnDisk(userId.getUserId(), query)
                : gifService.findGif(userId.getUserId(), query);
    }

    @PostMapping("/{userId}/generate")
    Path generateGif(@PathVariable @Valid UserIdDto userId, @RequestBody GifGenerationDto gifRequestDto) {
        return gifRequestDto.getForce()
                ? gifService.getFromGiphy(userId.getUserId(), gifRequestDto.getQuery())
                : gifService.getFromCache(userId.getUserId(), gifRequestDto.getQuery());
    }

    @DeleteMapping("/{userId}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserHistory(@PathVariable @Valid UserIdDto userId) {
        historyService.deleteUserHistory(userId.getUserId());
    }

    @DeleteMapping("/{userId}/reset")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserCacheInDictionary(@PathVariable @Valid UserIdDto userId, @RequestParam(required = false) String query) {
        if (query == null) {
            cacheService.deleteCacheFromMemory(userId.getUserId());
        } else {
            cacheService.deleteCacheFromMemoryByQuery(userId.getUserId(), query);
        }
    }

    @DeleteMapping("/{userId}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserCache(@PathVariable @Valid UserIdDto userId) {
        cacheService.deleteUserCacheFromDisk(userId.getUserId());
        cacheService.deleteCacheFromMemory(userId.getUserId());
    }
}
