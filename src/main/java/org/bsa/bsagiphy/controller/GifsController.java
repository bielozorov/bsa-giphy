package org.bsa.bsagiphy.controller;

import org.bsa.bsagiphy.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Path;
import java.util.List;

@RestController
public class GifsController {

    private final CacheService cacheService;

    @Autowired
    public GifsController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping("/gifs")
    public List<Path> getGifsList() {
        return cacheService.listAllFilesPaths();
    }

}
