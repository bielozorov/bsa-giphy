package org.bsa.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GifGenerationDto {
    private final String query;
    private final Boolean force;
}
