package org.bsa.bsagiphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.net.http.HttpClient;

@Configuration
public class ApiConfiguration {
    @Bean
    @Scope(value = "prototype") // singleton by default
    public HttpClient httpClient() {
        return HttpClient.newHttpClient();
    }
}
