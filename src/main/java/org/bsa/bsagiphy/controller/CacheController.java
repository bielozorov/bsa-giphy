package org.bsa.bsagiphy.controller;

import org.bsa.bsagiphy.dto.GifDto;
import org.bsa.bsagiphy.dto.GifGenerationDto;
import org.bsa.bsagiphy.service.CacheService;
import org.bsa.bsagiphy.service.GifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cache")
public class CacheController {
    private final GifService gifService;
    private final CacheService cacheService;

    @Autowired
    public CacheController(CacheService cacheService, GifService gifService) {
        this.cacheService = cacheService;
        this.gifService = gifService;
    }

    @GetMapping
    public List<GifDto> getCacheFromDisk(@RequestParam(required = false) String query) {
        return cacheService.getCacheFromDisk(query);
    }

    @PostMapping("/generate")
    public GifDto generateGif(@RequestBody GifGenerationDto gifGenerationDto) {
        return gifService.generateGif(gifGenerationDto.getQuery());
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCacheFromDisk() {
        cacheService.deleteAllCacheFromDisk();
    }

}
